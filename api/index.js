const { Keystone } = require('@keystonejs/keystone');
const { PasswordAuthStrategy } = require('@keystonejs/auth-password');
const { GraphQLApp } = require('@keystonejs/app-graphql');
const { AdminUIApp } = require('@keystonejs/app-admin-ui');
const initialiseData = require('./initial-data');

const item = require('./models/item');
const client = require('./models/client');
const user = require('./models/user');
const branch = require('./models/branch');
const package = require('./models/package');
const order = require('./models/order');
const element = require('./models/element');

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const { MongooseAdapter: Adapter } = require('@keystonejs/adapter-mongoose');
const PROJECT_NAME = 'NOMA app';
const adapterConfig = { mongoUri: process.env.MONGO_HOST };


console.log(process.env.MONGO_HOST);

const keystone = new Keystone({
  secureCookies: false,
  adapter: new Adapter(adapterConfig),
  onConnect: process.env.CREATE_TABLES !== 'true' && initialiseData,
  cookieSecret: "2bcc01a89c4bb05eee7b76e58ef9f0d639a8e35d00b4985b897a01a7e91bc244",
  cookie: {
    secure: false,//process.env.NODE_ENV === 'production', // Defaults to true in production
    maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
    sameSite: false,
  },
  sessionStore: new MongoStore({url: process.env.MONGO_HOST})
});

keystone.createList('User', user);
keystone.createList('Client',  client);
keystone.createList('Item',  item);
keystone.createList('Branch',  branch);
keystone.createList('Package',  new package(keystone).schema);
keystone.createList('Order',  order);
keystone.createList('Element',  new element(keystone).schema);


const authStrategy = keystone.createAuthStrategy({
  type: PasswordAuthStrategy,
  list: 'User',
});

module.exports = {
  keystone,
  apps: [
    new GraphQLApp(),
    new AdminUIApp({
      name: PROJECT_NAME,
      enableDefaultRoute: true,
      authStrategy,
      port: 3000
    }),
  ],
  configureExpress: app => {
    app.set('trust proxy', true);
  },
};


