const { Text, Float, Relationship, Virtual } = require('@keystonejs/fields');

class Package {
  constructor(keystone){
    this.keystone = keystone;

    this.schema = {
      fields: {
        label: {type: Text, isRequired: true},
        item: {type: Relationship, ref: "Item", many:false, isRequired: true},
        basePrice: {type: Float, isRequired: true},
        partnerPrice: {type: Float, isRequired: true},
        virtualLabel: {type: Virtual, resolver: async (value) => await this.getLabel(value)}
      },
      labelResolver: async (value) => await this.getLabel(value)
    }
  }

  async getLabel(value){
    const { data, errors } = await this.keystone.executeGraphQL({
      context: this.keystone.createContext({ skipAccessControl: true }),
      query: `
        query ($id: ID!){
          Item(where: {id: $id}){
            label
          }
        }
      `,
      variables: { id: String(value.item) },
    });
    if(errors) return "";
    return value.label + " - " + data.Item.label;
  }

}


module.exports = Package;
