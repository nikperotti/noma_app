const { Text, Float, Relationship } = require('@keystonejs/fields');

module.exports = {
  fields: {
    client: {type: Relationship, ref: "Client", many:false, isRequired: true},
    package: {type: Relationship, ref: "Package", many:false, isRequired: true},
  },
  labelResolver: ( ) => package.label
}


class Element {
  constructor(keystone){
    this.keystone = keystone;

    this.schema = {
      fields: {
        client: {type: Relationship, ref: "Client", many:false, isRequired: true},
        package: {type: Relationship, ref: "Package", many:false, isRequired: true},
        order: {type: Relationship, ref: "Order.elements", many:false, isRequired: true},
      },
      labelResolver: async (value) => await this.createLabel(value)
    }
  }

  async createLabel(value){
    const { data, errors } = await this.keystone.executeGraphQL({
      context: this.keystone.createContext({ skipAccessControl: true }),
      query: `
        query($packageID: ID!, $clientID: ID!){
          Client(where: {id: $clientID}){
            name,
            surname,
          }
          Package(where: {id: $packageID}){
            virtualLabel
          }
        }
      `,
      variables: { packageID: String(value.package), clientID: String(value.client)},
    });
    if(errors) return "";
    return data.Client.name + " " + data.Client.surname + " - " + data.Package.virtualLabel
  }

}


module.exports = Element;
