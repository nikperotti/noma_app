const { Float, Relationship, DateTime, Checkbox } = require('@keystonejs/fields');

module.exports = {
  fields: {
    operator: {type: Relationship, ref: 'User'},
    branch: {type: Relationship, ref: 'Branch'},
    elements: {type: Relationship, ref: 'Element.order', many: true},
    total: {type: Float, isRequired: true},
    start: {
      type: DateTime,
      format: 'dd/MM/yyyy HH:mm O',
      yearRangeFrom: 2018,
      yearRangeTo: 2030,
      yearPickerType: 'auto',
      isRequired: true
    },
    stop: {
      type: DateTime,
      format: 'dd/MM/yyyy HH:mm O',
      yearRangeFrom: 2018,
      yearRangeTo: 2030,
      yearPickerType: 'auto',
    },
    promo: {type: Checkbox, isRequired: true},
    free: {type: Checkbox, isRequired: true}
  },
}
