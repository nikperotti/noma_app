const { Text, Checkbox } = require('@keystonejs/fields');

module.exports = {
  fields: {
    name: {type: Text, isRequired: true},
    surname: {type: Text},
    email: {type: Text},
    taxcode: {type: Text},
    phoneNumber: {type: Text},
    partner: {type: Checkbox, isRequired: true},
  },
  labelResolver: (user) => user.name + " " + user.surname
}
