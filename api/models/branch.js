
const { Text } = require('@keystonejs/fields');

module.exports = {
  fields: {
    label: { type: Text },
    lat: { type: Text },
    lng: { type: Text },
  },
  labelResolver: (branch) => branch.label
}


