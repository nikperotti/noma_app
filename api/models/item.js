const { Text, Float, Relationship } = require('@keystonejs/fields');

module.exports = {
  fields: {
    label: {type: Text, isRequired: true},
    holdings: {type: Float, isRequired: true},
  },
  labelResolver: (item) => item.label
}
