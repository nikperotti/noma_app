import 'package:flutter/material.dart';

ThemeData appDefultTheme = ThemeData(
    primarySwatch: Colors.blue,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    iconTheme: IconThemeData(color: Colors.white),
    inputDecorationTheme: InputDecorationTheme(
      hintStyle: TextStyle(color: Colors.white),
      filled: true,
      fillColor: Color(0xff7fcdee),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(50)),
        borderSide: BorderSide(
          style: BorderStyle.solid,
          color: Color(0xff7fcdee),
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(50)),
        borderSide: BorderSide(
          style: BorderStyle.solid,
          color: Colors.white,
        ),
      ),
    ),
    textTheme: TextTheme(
        subtitle1: TextStyle(
            fontFamily: 'Montserrat', fontSize: 20.0, color: Colors.white),
        subtitle2: TextStyle(
            fontFamily: 'Montserrat', fontSize: 25.0, color: Colors.white)),
    buttonTheme: ButtonThemeData(
      buttonColor: Color(0xff7fcdee),
    ));
