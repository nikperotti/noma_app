import 'package:NOMA_app/data/model.dart';
import 'package:NOMA_app/data/order.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:NOMA_app/data/element.dart' as model;

class OrderBuilder extends StatefulWidget {
  OrderBuilder({Key key}) : super(key: key);
  @override
  _OrderBuilderState createState() => _OrderBuilderState();
}

class _OrderBuilderState extends State<OrderBuilder> {
  AppState appState;
  List orderElementDropdown;

  @override
  Widget build(BuildContext context) {
    this.appState = Provider.of<AppState>(context);
    if (this.appState.selectOrder == null) this.appState.selectOrder = Order();

    //Se non inizializto ritorna vuoto
    if (this.appState.packages == null) {
      return Container();
    }

    //Set di base se presenti elementi nuovi
    this
        .appState
        .selectOrder
        .elements
        .where((element) =>
            element == null ||
            element.package == null ||
            element.package.id == null)
        .forEach((e) => e.package = this.appState.packages != null
            ? this.appState.packages.first
            : null);

    //Costruisco la lista degli elementi
    List<Widget> composeScreen = this.appState.selectOrder.elements.map((el) {
      return buildElement(element: el);
    }).toList();

    //Aggiungo il pulsante + elementi
    composeScreen.add(addElement());

    //Aggiungo promo
    composeScreen.add(promoBox(this.appState.selectOrder));

    //Aggiungo etichetta totle
    composeScreen.add(totalPrice(this.appState.selectOrder));

    //Aggiungo tasto salva
    composeScreen.add(save());

    //Monto il componente
    return ListView(padding: EdgeInsets.only(top: 10), children: [
      Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: composeScreen)
    ]);
  }

  //Componente elemento
  Widget buildElement({model.Element element}) {
    return Container(
        constraints: BoxConstraints(minWidth: 300, maxWidth: 500),
        padding: EdgeInsets.only(bottom: 10),
        child: Stack(children: <Widget>[
          Card(
            shape: RoundedRectangleBorder(
                side: BorderSide(
                  color: Colors.black38,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            key: null,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //Cliente
                Container(
                  child: RaisedButton(
                      onPressed: () => null,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text((element.client == null ||
                              element.client.name == null)
                          ? "Aggiungi cliente"
                          : element.client.name +
                              " " +
                              element.client.surname)),
                  padding: EdgeInsets.all(15),
                ),
                //Oggetto
                dropdownSelect(
                    valueSelect:
                        element.package != null ? element.package.item.id : "",
                    onChanege: (value) {
                      element.package = this
                          .appState
                          .packages
                          .firstWhere((el) => el.item.id == value);
                      setState(() {
                        appState = this.appState;
                      });
                    },
                    values: this.appState.items,
                    label: "Oggetto",
                    icon: Icons.category),
                //Pacchetto
                dropdownSelect(
                    valueSelect: element.package.id,
                    onChanege: (value) {
                      element.package = this
                          .appState
                          .packages
                          .firstWhere((el) => el.id == value);
                      setState(() {
                        appState = this.appState;
                      });
                    },
                    values: this
                        .appState
                        .packages
                        .where((el) => element.package != null
                            ? el.item.id == element.package.item.id
                            : true)
                        .toList(),
                    label: "Pacchetto",
                    icon: Icons.inbox),
                Container(
                  child: RaisedButton(
                      onPressed: () {
                        Provider.of<AppState>(context, listen: false)
                            .selectOrder = this.appState.selectOrder;
                        Navigator.of(context).pushNamed('/signature');
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text("Firma Documento")),
                  padding: EdgeInsets.only(bottom: 15),
                ),
              ],
            ),
          ),
          //Elimina elemento
          Positioned(
            top: 0,
            right: 0,
            child: IconButton(
              onPressed: () {
                this.appState.selectOrder.elements.remove(element);
                setState(() {
                  appState = this.appState;
                });
              },
              icon: Icon(Icons.close, color: Colors.black38),
            ),
          )
        ]));
  }

  //Componente dropDown
  Widget dropdownSelect(
      {String valueSelect,
      Function onChanege,
      List values,
      String label,
      IconData icon}) {
    return Container(
        padding: EdgeInsets.only(bottom: 15, left: 15, right: 15),
        constraints: BoxConstraints(minWidth: 200, maxWidth: 400),
        child: DropdownButtonFormField(
            decoration: InputDecoration(
                labelText: label,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey))),
            icon: Icon(icon),
            value: valueSelect,
            style: TextStyle(color: Colors.black),
            focusColor: Colors.black,
            items: values.map((value) {
              return new DropdownMenuItem<String>(
                value: value.id,
                child: new Text(value.label),
              );
            }).toList(),
            onChanged: onChanege));
  }

  //Componente aggiungi elemento
  Widget addElement() {
    return Container(
      constraints: BoxConstraints(minWidth: 300, maxWidth: 500),
      child: RaisedButton(
        onPressed: () {
          this.appState.selectOrder.elements.add(model.Element());
          setState(() {
            appState = this.appState;
          });
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Icon(Icons.add),
      ),
    );
  }

  //Checkbox promo
  Widget promoBox(Order order) {
    return Container(
        constraints: BoxConstraints(minWidth: 100, maxWidth: 300),
        child: CheckboxListTile(
          title: Text("Promo", style: TextStyle(color: Colors.black)),
          value: order.promo,
          onChanged: (newValue) {
            order.promo = newValue;
            setState(() {
              appState = this.appState;
            });
          },
        ));
  }

  //Etichetta totale
  Widget totalPrice(Order order) {
    return Container(
        constraints: BoxConstraints(minWidth: 300, maxWidth: 400),
        padding: EdgeInsets.only(bottom: 15, left: 15, right: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Prezzo totale:"),
            order.promo
                ? SizedBox(
                    child: TextField(
                        style: TextStyle(fontSize: 12, color: Colors.black),
                        decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(color: Colors.black)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(color: Colors.black)),
                            fillColor: Colors.white,
                            focusColor: Colors.black)),
                    width: 50,
                    height: 40,
                  )
                : Text(order.total.toString()),
            Text("€")
          ],
        ));
  }

  //Componente aggiungi elemento
  Widget save() {
    return Container(
      constraints: BoxConstraints(minWidth: 300, maxWidth: 500),
      child: RaisedButton(
        onPressed: () => null,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Text("Salva"),
      ),
    );
  }
}
