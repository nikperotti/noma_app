import 'package:NOMA_app/components/loading.dart';
import 'package:NOMA_app/data/client.dart';
import 'package:NOMA_app/data/model.dart';
import 'package:NOMA_app/graphql/searchClients.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class ClientBuild extends StatefulWidget {
  ClientBuild({Key key}) : super(key: key);
  @override
  _ClientBuildState createState() => _ClientBuildState();
}

class _ClientBuildState extends State<ClientBuild> {
  Client clientSearch = Client();
  AppState appState;
  _ClientBuildState();

  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.appState = Provider.of<AppState>(context);

    if (this.appState.selectClient != null) {
      return builderClient(client: this.appState.selectClient);
    }

    return buildSearchClient();
  }

  Widget buildSearchClient() {
    return ListView(padding: EdgeInsets.only(top: 10), children: [
      Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [builderClient(), builderSearch()])
    ]);
  }

  Widget builderClient({Client client}) {
    if (client == null) {
      return Container(
          constraints: BoxConstraints(minWidth: 300, maxWidth: 500),
          padding: EdgeInsets.only(bottom: 10),
          child: Card(
            shape: RoundedRectangleBorder(
                side: BorderSide(
                  color: Colors.black38,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            key: null,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                textInput(
                    label: "Nome",
                    topPadding: true,
                    change: (data) => this.setState(() {
                          this.clientSearch.name = data;
                        })),
                textInput(
                    label: "Cognome",
                    change: (data) => this.setState(() {
                          this.clientSearch.surname = data;
                        })),
                textInput(
                    label: "Email",
                    change: (data) => this.setState(() {
                          this.clientSearch.email = data;
                        })),
                textInput(
                    label: "Telefono",
                    change: (data) => this.setState(() {
                          this.clientSearch.phoneNumber = data;
                        })),
                textInput(
                    label: "Codice fiscale",
                    change: (data) => this.setState(() {
                          this.clientSearch.taxcode = data;
                        })),
                checkboxInput(label: "Socio")
              ],
            ),
          ));
    } else {}
  }

  Widget textInput({String label, bool topPadding = false, var change}) {
    return Container(
        padding: EdgeInsets.only(
            bottom: 15, top: topPadding ? 15 : 0, left: 15, right: 15),
        child: TextField(
            onChanged: change,
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
                labelStyle: TextStyle(color: Colors.black),
                labelText: label,
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder())));
  }

  Widget checkboxInput({String label, bool topPadding = false}) {
    return Container(
        padding: EdgeInsets.only(
            bottom: 15, top: topPadding ? 15 : 0, left: 15, right: 15),
        child: CheckboxListTile(
            onChanged: (data) => this.setState(() {
                  this.clientSearch.partner = data;
                }),
            title: Text(label, style: TextStyle(color: Colors.black)),
            value: this.clientSearch.partner));
  }

  Widget builderSearch() {
    if ((this.clientSearch.name == null || this.clientSearch.name.length < 3) &&
        (this.clientSearch.surname == null ||
            this.clientSearch.surname.length < 3)) return Container();
    return Query(
      options: QueryOptions(documentNode: gql(searchClients), variables: {
        'name': this.clientSearch.name,
        'surname': this.clientSearch.surname
      }),
      builder: (QueryResult result, {void refetch, FetchMore fetchMore}) {
        if (result.data != null) {
          List<Client> clients = Client.fromListMap(result.data['allClients']);
          return Column(
              children: clients.map((c) {
            return searchClientBuilder(c);
          }).toList());
        }
        if (result.exception != null) {
          print(result.exception);
          //return Loading(callback: () async => await Navigator.of(context).pushReplacementNamed('/logout'));
        }
        return Loading(style: Style.LIGHT);
      },
    );
  }

  Widget searchClientBuilder(Client client) {
    if (client == null || client.name == null) return Container();
    return Container(
        constraints: BoxConstraints(minWidth: 300, maxWidth: 500),
        child: Card(
          margin: EdgeInsets.only(bottom: 10),
          shape: RoundedRectangleBorder(
              side: BorderSide(
                color: Colors.black38,
                width: 1.0,
              ),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          key: null,
          child: Container(
              margin: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(client.name, style: TextStyle(color: Colors.black)),
                      Text(" "),
                      Text(client.surname)
                    ],
                  ),
                  FlatButton(
                    onPressed: () => null,
                    child: Text("Seleziona"),
                  )
                ],
              )),
        ));
  }
}
