import 'package:NOMA_app/data/model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  final AppState appState;

  CustomDrawer({this.appState});

  @override
  Drawer build(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
            padding: EdgeInsets.zero,
            child: Container(
              padding: EdgeInsets.only(top: 16),
              color: Colors.blue,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlutterLogo(size: 60),
                  Text(appState.user.name + " " + appState.user.surname,
                      style: Theme.of(context).textTheme.subtitle1),
                  Text(appState.user.email,
                      style: Theme.of(context).textTheme.subtitle1),
                ],
              ),
            )),
        ListTile(
          title: Text("Ordini"),
          leading: Icon(Icons.assignment),
          onTap: () => Navigator.of(context).pushNamed('/orders'),
        ),
        ListTile(
          title: Text("Abbonamento"),
          leading: Icon(Icons.assignment_turned_in),
          onTap: () {},
        ),
        ListTile(
          title: Text("Clienti"),
          leading: Icon(Icons.assignment_ind),
          onTap: () => Navigator.of(context).pushNamed('/clients'),
        ),
        ListTile(
          title: Text("Contabilità"),
          leading: Icon(Icons.attach_money),
          onTap: () {},
        ),
        ListTile(
          title: Text("Account"),
          leading: Icon(Icons.account_circle),
          onTap: () {},
        ),
        ListTile(
          title: Text("Logout"),
          leading: Icon(Icons.exit_to_app),
          onTap: () {
            Navigator.of(context).pushReplacementNamed('/logout');
          },
        )
      ],
    ));
  }
}
