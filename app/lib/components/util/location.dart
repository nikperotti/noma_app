import 'package:location/location.dart';
import 'dart:math' show cos, sqrt, asin;

Future<String> getBranchLocation(List branchs) async {
  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;

  _serviceEnabled = await location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await location.requestService();
    if (!_serviceEnabled) {
      return null;
    }
  }

  _permissionGranted = await location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return null;
    }
  }
  LocationData _locationData =
      await location.getLocation().timeout(Duration(seconds: 30));

  /*Positizione vicino a sbt _locationData =
      LocationData.fromMap({'latitude': 42.937121, 'longitude': 13.891769});*/

  return caluclateMinDistance(branchs, _locationData);
}

String caluclateMinDistance(List branchs, LocationData position) {
  List<Map<String, dynamic>> branchsPosition = branchs
      .map((e) => {
            'id': e['id'],
            'position': LocationData.fromMap({
              'latitude': double.parse(e['lat']),
              'longitude': double.parse(e['lng'])
            })
          })
      .toList();
  List<Map<String, dynamic>> result = branchsPosition
      .map((e) => {
            'id': e['id'],
            'distance': calculateDistance(e['position'], position)
          })
      .toList();
  result.sort((a, b) {
    return ((a['distance'] - b['distance']) * 100).round();
  });
  return result.first['id'];
}

double calculateDistance(LocationData branch, LocationData position) {
  var p = 0.017453292519943295;
  var c = cos;
  var a = 0.5 -
      c((position.latitude - branch.latitude) * p) / 2 +
      c(branch.latitude * p) *
          c(position.latitude * p) *
          (1 - c((position.longitude - branch.longitude) * p)) /
          2;
  return 12742 * asin(sqrt(a));
}
