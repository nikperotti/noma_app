import 'package:NOMA_app/components/loading.dart';
import 'package:NOMA_app/components/util/location.dart';
import 'package:NOMA_app/data/model.dart';
import 'package:NOMA_app/data/package.dart';
import 'package:NOMA_app/graphql/getOrderBaseData.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:universal_html/prefer_universal/js.dart';
import 'package:NOMA_app/data/item.dart';

Widget confermCard(
    {String query,
    Map<String, dynamic> values,
    String title,
    String okTitle,
    String closeTitle,
    Function close,
    Function ok,
    BuildContext context}) {
  return Mutation(
      options: MutationOptions(documentNode: gql(query)),
      builder: (RunMutation run, QueryResult result) {
        if (result.loading) return Loading();
        if (result.exception != null) {
          print(result.exception);
          return Loading(
              callback: () async =>
                  await Navigator.of(context).pushReplacementNamed('/logout'));
        }
        if (result.data != null) {
          return Loading(callback: () async => await close());
        }
        return Center(
            child: Card(
          margin: EdgeInsets.all(15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                title: Text(
                  title,
                  style: TextStyle(color: Colors.black),
                ),
              ),
              ButtonBar(
                children: <Widget>[
                  FlatButton(
                    child: Text(closeTitle),
                    onPressed: close,
                  ),
                  RaisedButton(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(okTitle),
                    ),
                    onPressed: () => run(values),
                  )
                ],
              ),
            ],
          ),
        ));
      });
}

Widget branchSelctCard(Function setBranch, context) {
  var branch = null;
  List branchs = [];
  return Query(
      options: QueryOptions(documentNode: gql(getOrderBaseData)),
      builder: (QueryResult result, {void refetch, FetchMore fetchMore}) {
        if (result.loading) return Loading();
        if (result.data != null) {
          Provider.of<AppState>(context, listen: false).packages =
              Package.fromListMap(result.data['allPackages']);
          Provider.of<AppState>(context, listen: false).items =
              Item.fromListMap(result.data['allItems']);
          getBranchLocation(result.data['allBranches']).then(setBranch);
          branchs = result.data['allBranches'];
          Provider.of<AppState>(context, listen: false).branchs =
              result.data['allBranches'];
          return Center(
              child: Card(
            margin: EdgeInsets.all(15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.all(16),
                    child: DropdownButtonFormField<String>(
                        decoration: InputDecoration(
                            labelText: "Seleziona la Sede",
                            fillColor: Colors.transparent,
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey))),
                        icon: Icon(Icons.room),
                        value: branch,
                        style: TextStyle(color: Colors.black),
                        focusColor: Colors.black,
                        items: branchs.map((value) {
                          return new DropdownMenuItem(
                            value: value['id'].toString(),
                            child: new Text(value['label'].toString()),
                          );
                        }).toList(),
                        onChanged: (value) => {branch = value})),
                ButtonBar(
                  children: <Widget>[
                    RaisedButton(
                      child: Container(
                          padding: EdgeInsets.all(10),
                          child: Text("Seleziona")),
                      onPressed: () => setBranch(branch),
                    ),
                  ],
                )
              ],
            ),
          ));
        }
        return Loading();
      });
}
