import 'package:NOMA_app/data/model.dart';
import 'package:flutter/material.dart';

bool isLoggedRedirect(BuildContext context, AppState appState) {
  if (appState.user.isLogged() == false) {
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => Navigator.of(context).pushReplacementNamed('/login'));
    return true;
  }
  return false;
}
