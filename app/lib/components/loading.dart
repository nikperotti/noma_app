import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

enum Style { DARK, LIGHT }

class Loading extends StatelessWidget {
  final Function callback;
  final Style style;

  Loading({this.callback, this.style = Style.DARK});
  @override
  Widget build(BuildContext context) {
    if (callback != null) {
      WidgetsBinding.instance
          .addPostFrameCallback((_) async => await this.callback());
    }
    return Container(
      color: this.style == Style.DARK
          ? Color(0xff29abe2)
          : Theme.of(context).scaffoldBackgroundColor,
      child: Center(
        child: SpinKitRing(
          color: this.style == Style.DARK ? Colors.white : Color(0xff29abe2),
          size: 50.0,
        ),
      ),
    );
  }
}
