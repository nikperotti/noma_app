import 'package:NOMA_app/components/util/confermCard.dart';
import 'package:NOMA_app/data/model.dart';
import 'package:NOMA_app/data/order.dart';
import 'package:NOMA_app/graphql/openCloseOrder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';

class OrderList extends StatefulWidget {
  OrderList(
      {Key key,
      this.changeBranch,
      this.orders,
      this.setOverlay,
      this.setBranch,
      this.refresh})
      : super(key: key);
  List orders;
  Function changeBranch;
  Function setOverlay;
  Function setBranch;
  Function refresh;
  @override
  _OrderListState createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  Function changeBranch;
  List orders;
  int indexDivider;
  AppState appState;

  _OrderListState();

  initState() {
    List openOrder =
        widget.orders.where((element) => element['stop'] == null).toList();
    List closeOrder =
        widget.orders.where((element) => element['stop'] != null).toList();
    this.indexDivider = openOrder.length;
    this.orders = [];
    this.orders.addAll(openOrder);
    this.orders.addAll(closeOrder);
    super.initState();
  }

  build(BuildContext context) {
    this.appState = this.appState = Provider.of<AppState>(context);

    return RefreshIndicator(
        child: ListView.builder(
          itemCount: this.orders.length + 1,
          itemBuilder: (context, index) => item(index - 1),
        ),
        onRefresh: widget.refresh);
  }

  Widget selectBranch() {
    return DropdownButtonFormField<String>(
        decoration: InputDecoration(
            labelText: "Seleziona la Sede",
            fillColor: Colors.transparent,
            enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey))),
        icon: Icon(Icons.room),
        value: this.appState.branch,
        style: TextStyle(color: Colors.black),
        focusColor: Colors.black,
        items: this.appState.branchs.map((value) {
          return new DropdownMenuItem(
            value: value['id'].toString(),
            child: new Text(value['label'].toString()),
          );
        }).toList(),
        onChanged: (value) {
          widget.setBranch(value);
        });
  }

  Widget item(index) {
    if (index == -1) return selectBranch();
    var order = orders[index];
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actions: order['stop'] == null
          ? <Widget>[
              IconSlideAction(
                caption: 'Chiudi',
                color: Colors.red,
                icon: Icons.close,
                onTap: () {
                  widget.setOverlay(queryOverlay('close', order));
                  //widget.refresh();
                },
              ),
            ]
          : <Widget>[
              IconSlideAction(
                caption: 'Apri',
                color: Colors.green,
                icon: Icons.add_box,
                onTap: () {
                  widget.setOverlay(queryOverlay('open', order));
                  //widget.refresh();
                },
              ),
            ],
      child: text(order),
      secondaryActions: this.appState.user.isAdmin
          ? <Widget>[
              IconSlideAction(
                caption: 'Modifica',
                color: Colors.blue,
                icon: Icons.edit,
                onTap: () => setModOrder(order),
              ),
              IconSlideAction(
                caption: 'Delete',
                color: Colors.red,
                icon: Icons.delete,
                onTap: () => widget.setOverlay(queryOverlay('delate', order)),
              ),
            ]
          : [
              IconSlideAction(
                caption: 'Modifica',
                color: Colors.blue,
                icon: Icons.edit,
                onTap: () => setModOrder(order),
              ),
            ],
    );
  }

  text(order) {
    return GestureDetector(
      onTap: () => {},
      child: Container(
        color: Colors.white,
        child: Card(
            child: ListTile(
          leading: Container(
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              width: 50,
              height: 300.0,
              decoration: BoxDecoration(
                  color: order["stop"] == null ? Colors.green : Colors.red,
                  shape: BoxShape.circle)),
          title: createTitleLabel(order),
          subtitle: createSubtitleLabel(order),
        )),
      ),
    );
  }

  Widget createTitleLabel(data) {
    var label = "";
    data['elements'].forEach((element) => {
          if (label.length == 0)
            {label = element['client']["_label_"]}
          else
            {label = label + " | " + element['client']["_label_"]}
        });
    return Text(label, style: TextStyle(color: Colors.black));
  }

  Widget createSubtitleLabel(data) {
    var label = "";
    data['elements'].forEach((element) => {
          if (label.length == 0)
            {label = element['package']["_label_"]}
          else
            {label = label + " | " + element['package']["_label_"]}
        });
    return Text(label, style: TextStyle(color: Colors.black));
  }

  Widget queryOverlay(String type, order) {
    switch (type) {
      case 'close':
        return confermCard(
          query: openCloseOrder,
          values: {
            "id": order['id'].toString(),
            "stop": DateTime.now().toIso8601String()
          },
          context: this.context,
          title: "Sei sicuro di voler chiudere l'ordine?",
          close: () {
            widget.setOverlay(null);
            widget.refresh();
          },
          okTitle: "Chiudi",
          closeTitle: "Annulla",
        );
      case 'open':
        return confermCard(
          query: openCloseOrder,
          values: {"id": order['id'].toString(), "stop": null},
          context: this.context,
          title: "Sei sicuro di voler aprire l'ordine?",
          close: () {
            widget.setOverlay(null);
            widget.refresh();
          },
          okTitle: "Apri",
          closeTitle: "Annulla",
        );
    }
    return null;
  }

  setModOrder(orderMap) {
    Provider.of<AppState>(context, listen: false).selectOrder =
        Order.formMap(orderMap);
    Navigator.of(context).pushNamed('/order');
  }
}
