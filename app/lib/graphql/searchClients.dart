const searchClients = """
query searchClients(\$name: String, \$surname: String){
  allClients(where: { AND :[
		{name_starts_with: \$name},
    {surname_starts_with: \$surname}]
  }){
    _label_
    id
    name
    surname
		email
    taxcode
    phoneNumber
    partner
  }
}
""";
