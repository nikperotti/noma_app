String getOrderBaseData = """
query getOrderBaseData{
  allBranches{
    id
    label
    lat
    lng
  }
  
	allPackages{
    id
    label
    item{
      id
      label
      holdings
    }
    basePrice
    partnerPrice
  }
  
  allItems{
    id
    label
    holdings
  }
}
""";
