String getPackages = """query getPackages{
	allPackages{
    id
    label
    item{
      id
      label
      holdings
    }
    basePrice
    partnerPrice
  }
}""";
