const mutationLogin = """mutation login(\$email: String, \$password: String){
  authenticateUserWithPassword(email: \$email, password: \$password){
    token,
    item{
      id
      name
      email
      isAdmin
    }
  }
}""";
