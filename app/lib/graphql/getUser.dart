const getUser = """
  query getUser(\$id: ID!){ 
    User(where:{id: \$id}){
      id
      name
      surname
      email
      isAdmin
    }
  }
""";
