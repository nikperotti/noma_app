String openCloseOrder = """mutation closeOrder(\$id: ID!, \$stop: DateTime){
  updateOrder(id:\$id, data:{stop: \$stop}){
    id
  }
}""";
