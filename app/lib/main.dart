import 'package:NOMA_app/page/client.dart';
import 'package:NOMA_app/page/clients.dart';
import 'package:NOMA_app/page/login.dart';
import 'package:NOMA_app/page/logout.dart';
import 'package:NOMA_app/page/order.dart';
import 'package:NOMA_app/page/orders.dart';
import 'package:NOMA_app/page/signature.dart';

import 'package:NOMA_app/util/theme.dart';

import 'package:flutter/material.dart';
//import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

import 'data/model.dart';

void main() async {
  /*Inizializzaizone cookie e AppSate*/
  WebStorage();
  AppState();

  WidgetsFlutterBinding.ensureInitialized();

  //Connettore GraphQL
  final Link httpLink = HttpLink(
    uri: 'http://api.noma-lni.it/admin/api',
  );
  final AuthLink authLink = AuthLink(
    getToken: () async => 'Bearer ' + WebStorage.instance.token.toString() + '',
  );
  final Link link = authLink.concat(httpLink);
  final policies = Policies(
    fetch: FetchPolicy.networkOnly,
  );
  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      cache: InMemoryCache(),
      link: link,
      defaultPolicies: DefaultPolicies(
        watchQuery: policies,
        query: policies,
        mutate: policies,
      ),
    ),
  );

  //Start app
  runApp(MyApp(client));
}

class MyApp extends StatelessWidget {
  final ValueNotifier<GraphQLClient> client;
  MyApp(this.client);

  @override
  Widget build(BuildContext context) {
    //Connettore provider
    return Provider<AppState>(
      create: (_) => AppState.instance,
      child: GraphQLProvider(
          client: this.client,
          child: MaterialApp(
            title: 'Flutter Demo',
            theme: appDefultTheme,
            initialRoute: '/login',
            //Rotte
            routes: {
              '/': (context) => fixBackIos(child: Login(), context: context),
              '/login': (context) =>
                  fixBackIos(child: Login(), context: context),
              '/logout': (context) => Logout(),
              '/orders': (context) =>
                  fixBackIos(child: Orders(), context: context),
              '/order': (context) =>
                  fixBackIos(child: Order(), context: context),
              '/signature': (context) =>
                  fixBackIos(child: Signature(), context: context),
              '/clients': (context) =>
                  fixBackIos(child: Clients(), context: context),
              '/client': (context) =>
                  fixBackIos(child: Client(), context: context)
            },
          )),
    );
  }
}

//Fixer ios back
Widget fixBackIos({Widget child, context}) {
  return WillPopScope(
      onWillPop: () async => !Navigator.of(context).userGestureInProgress,
      child: child);
}
