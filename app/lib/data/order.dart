import 'package:NOMA_app/data/element.dart';
import 'package:NOMA_app/data/user.dart';

class Order {
  String id;
  User operator;
  String branchId;
  List<Element> elements = [];
  int total;
  DateTime start;
  DateTime stop;
  bool promo = false;
  bool free;

  Order() {
    this.elements.add(Element());
  }

  Order.formMap(Map<String, dynamic> map) {
    this.id = map['id'] ?? this.id;
    this.operator = User.formMap(map["operator"]) ?? this.operator;
    this.elements = Element.fromListMap(map['elements']) ?? this.elements;
    this.total = map['total'] ?? this.total;
    this.start = DateTime.parse(map['start']) ?? this.start;
    this.stop = map['stop'] != null ? DateTime.parse(map['stop']) : this.stop;
    this.promo = map['promo'] ?? this.promo;
    this.free = map['free'] ?? this.free;
  }
}
