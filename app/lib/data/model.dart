import 'dart:convert';

import 'package:NOMA_app/data/item.dart';
import 'package:NOMA_app/data/order.dart';
import 'package:NOMA_app/data/package.dart';
import 'package:NOMA_app/data/user.dart';
import 'package:NOMA_app/data/client.dart' as model;

import 'package:universal_html/prefer_universal/html.dart';

class AppState {
  User user = User();
  String branch = null;
  List branchs = [];
  Order selectOrder;
  model.Client selectClient;
  List<Package> packages;
  List<Item> items;

  AppState._internal();
  static final AppState instance = AppState._internal();

  factory AppState() {
    return instance;
  }

  logout() {
    this.user = User();
  }

  Map<String, dynamic> toMap() {
    return {'user': this.user.toMap()};
  }

  String toJson() {
    return jsonEncode(this.toMap());
  }

  AppState.formMap(Map<String, dynamic> map) {
    this.user = User.formMap(map['user']) ?? this.user;
  }
}

class WebStorage {
  WebStorage._internal();
  static final WebStorage instance = WebStorage._internal();

  factory WebStorage() {
    return instance;
  }

  String get userId => window.localStorage['userId'];
  set userId(String userIds) => (userIds == null)
      ? window.localStorage.remove('userId')
      : window.localStorage['userId'] = userIds;

  String get token => window.localStorage['token'];
  set token(String tokens) => (tokens == null)
      ? window.localStorage.remove('token')
      : window.localStorage['token'] = tokens;
}
