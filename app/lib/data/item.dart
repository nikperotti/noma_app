class Item {
  String id;
  String label;
  int holdings;

  static fromListMap(List listMap) {
    List<Item> appList = [];
    listMap.forEach((element) => appList.add(Item.fromMap(element)));
    return appList;
  }

  Item.fromMap(Map<String, dynamic> map) {
    this.id = map['id'] ?? this.id;
    this.label = map['label'] ?? this.label;
    this.holdings = map['holdings'] ?? this.holdings;
  }
}
