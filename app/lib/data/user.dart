class User {
  String id;
  String name = "";
  String surname = "";
  String email = "";
  bool isAdmin = false;

  User({this.id, this.name, this.surname, this.email, this.isAdmin});

  bool isLogged() => this.name == null ? false : true;

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'name': this.name,
      'surname': this.surname,
      'email': this.email,
      'isAdmin': this.isAdmin,
    };
  }

  User.formMap(Map<String, dynamic> map) {
    if (map == null) return;
    this.id = map['id'] ?? this.id;
    this.name = map['name'] ?? this.name;
    this.surname = map['surname'] ?? this.surname;
    this.email = map['email'] ?? this.email;
    this.isAdmin = map['isAdmin'] ?? this.isAdmin;
  }
}
