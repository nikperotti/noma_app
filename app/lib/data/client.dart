class Client {
  String name;
  String surname;
  String email;
  String taxcode;
  String phoneNumber;
  bool partner;

  Client() {
    this.partner = false;
  }

  Client.fromMap(Map<String, dynamic> map) {
    this.name = map['name'] ?? this.name;
    this.surname = map['surname'] ?? this.surname;
    this.email = map['email'] ?? this.email;
    this.taxcode = map['taxcode'] ?? this.taxcode;
    this.phoneNumber = map['phoneNumber'] ?? this.phoneNumber;
    this.partner = map['partner'] ?? this.partner;
  }

  static fromListMap(List listMap) {
    List<Client> appList = [];
    listMap.forEach((element) => appList.add(Client.fromMap(element)));
    return appList;
  }
}
