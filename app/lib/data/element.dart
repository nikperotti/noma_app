import 'package:NOMA_app/data/client.dart';
import 'package:NOMA_app/data/package.dart';

class Element {
  String id;
  Client client;
  Package package;

  Element() {
    this.client = Client();
  }

  static fromListMap(List listMap) {
    List<Element> appList = [];
    listMap.forEach((element) => appList.add(Element.fromMap(element)));
    return appList;
  }

  Element.fromMap(Map<String, dynamic> map) {
    this.id = map['id'] ?? this.id;
    this.client = Client.fromMap(map['client']) ?? this.client;
    this.package = Package.fromMap(map['package']) ?? this.package;
  }
}
