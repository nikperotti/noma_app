import 'package:NOMA_app/data/item.dart';

class Package {
  String id;
  String label;
  int basePrice;
  int partnerPrice;
  Item item;

  Package();

  static fromListMap(List listMap) {
    List<Package> appList = [];
    listMap.forEach((element) => appList.add(Package.fromMap(element)));
    return appList;
  }

  Package.fromMap(Map<String, dynamic> map) {
    this.id = map['id'] ?? this.id;
    this.label = map['label'] ?? this.label;
    this.basePrice = map['basePrice'] ?? this.basePrice;
    this.partnerPrice = map['partnerPrice'] ?? this.partnerPrice;
    this.item = Item.fromMap(map['item']) ?? this.item;
  }
}
