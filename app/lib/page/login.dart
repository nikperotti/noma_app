import 'package:NOMA_app/data/model.dart';
import 'package:NOMA_app/data/user.dart';
import 'package:NOMA_app/graphql/getUser.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:NOMA_app/graphql/login.dart';

import 'package:NOMA_app/components/loading.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  //Stato email e password
  Map<String, dynamic> variables;
  @override
  initState() {
    this.variables = {"email": "", "password": ""};
    super.initState();
  }

  //Funzione cambio dati
  changeData(String label, data) {
    variables[label.toLowerCase()] = data;
  }

  //Campi email e password
  Widget field(
      {bool isPassword = false,
      String label = "",
      Function changeCallBack,
      String errorText,
      List<String> autoComplete,
      TextInputType keyboardType}) {
    return TextFormField(
      autofillHints: autoComplete,
      obscureText: isPassword,
      keyboardType: keyboardType,
      onChanged: (data) => changeCallBack(label, data),
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: label,
          errorText: errorText,
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }

  //Bottone login
  Widget button({String label = "", Function onPressed}) {
    return SizedBox(
        width: double.infinity,
        child: RaisedButton(
            onPressed: () => onPressed(),
            padding: const EdgeInsets.all(15.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: Text(
              label,
              style: Theme.of(context).textTheme.subtitle2,
              textAlign: TextAlign.center,
            )));
  }

  //Builder interfaccia login
  Scaffold builderLogin({Function callback, String notify}) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Container(
          color: Color(0xff29abe2),
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 155.0,
                  child: Image.asset(
                    "assets/img/icon.png",
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(height: 45.0),
                field(
                    label: "Email",
                    changeCallBack: this.changeData,
                    errorText: notify,
                    autoComplete: [AutofillHints.username],
                    keyboardType: TextInputType.emailAddress),
                SizedBox(height: 25.0),
                field(
                    label: "Password",
                    isPassword: true,
                    changeCallBack: this.changeData,
                    errorText: notify,
                    autoComplete: [AutofillHints.password]),
                SizedBox(
                  height: 35.0,
                ),
                button(label: "Login", onPressed: callback),
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //Aquisizione id utente da cookie
    String userId = WebStorage.instance.userId;

    //Se non esiste
    if (userId == null) {
      return loginScreen();
    }

    //Caricameno utente da cookie
    return Query(
      options: QueryOptions(
          documentNode: gql(getUser), variables: {'id': userId.toString()}),
      builder: (QueryResult result, {void refetch, FetchMore fetchMore}) {
        //Se utente trovato
        if (result != null && result.data != null) {
          //Ordini
          Provider.of<AppState>(context).user =
              User.formMap(result.data['User']);
          return Loading(
              callback: () async => {
                    await Navigator.of(context).pushReplacementNamed('/orders')
                  });
        }
        //Se utente non trovato
        else if (result.exception != null) {
          print(result.exception);
          //Logout
          return Loading(
              callback: () async =>
                  await Navigator.of(context).pushReplacementNamed('/logout'));
        }
        return Loading();
      },
    );
  }

  //Schermata login
  loginScreen() {
    return Mutation(
      options: MutationOptions(
        documentNode: gql(mutationLogin),
      ),
      builder: (RunMutation run, QueryResult result) {
        //Caricamento
        if (result.loading) {
          return Loading();
        }
        //Accesso consentito
        if (result.data != null &&
            result.data["authenticateUserWithPassword"]['item'] != null) {
          //Salvo stato utente
          Provider.of<AppState>(context).user =
              User.formMap(result.data["authenticateUserWithPassword"]['item']);
          //Salvo token e id utente in cookie
          Function callbackLoading = () async => {
                WebStorage.instance.token =
                    result.data["authenticateUserWithPassword"]['token'],
                WebStorage.instance.userId =
                    result.data["authenticateUserWithPassword"]['item']['id'],
                Navigator.of(context).pushReplacementNamed('/orders'),
              };
          return Loading(callback: callbackLoading);
        }
        //Password Errata
        if (result.exception != null) {
          print(result.exception);
          return builderLogin(
              callback: () => run(this.variables),
              notify: "Email o password errate");
        }
        return builderLogin(callback: () => run(this.variables));
      },
    );
  }
}
