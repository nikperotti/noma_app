import 'package:NOMA_app/components/customDrawer.dart';
import 'package:NOMA_app/components/loading.dart';
import 'package:NOMA_app/components/orderList.dart';
import 'package:NOMA_app/components/util/confermCard.dart';
import 'package:NOMA_app/components/util/redirect.dart';
import 'package:NOMA_app/data/model.dart';
import 'package:NOMA_app/graphql/getOrders.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class Orders extends StatefulWidget {
  Orders({Key key}) : super(key: key);
  @override
  _Ordersstate createState() => _Ordersstate();
}

class _Ordersstate extends State<Orders> {
  String branch;
  Widget branchSelect;

  Widget overlay;
  Function refetch;

  _Ordersstate();
  AppState appState;

  initState() {
    this.branchSelect = branchSelctCard(setBranch, context);
    this.overlay = branchSelect;

    super.initState();
  }

  setOverlay(Widget overlay) {
    setState(() {
      this.overlay = overlay;
    });
    if (this.refetch != null) {
      this.refetch();
    }
  }

  @override
  Widget build(BuildContext context) {
    this.appState = Provider.of<AppState>(context);
    this.branch = this.appState.branch;

    if (this.branch != null && this.overlay == this.branchSelect)
      this.overlay = null;

    if (isLoggedRedirect(context, this.appState)) return Loading();

    return Stack(
      children: this.overlay != null
          ? [
              ordini(),
              Container(color: Colors.black.withOpacity(0.5)),
              this.overlay
            ]
          : [ordini()],
    );
  }

  Widget ordini() {
    return Scaffold(
      drawerEdgeDragWidth: 0,
      drawer: CustomDrawer(appState: this.appState),
      appBar: AppBar(
          leading: Builder(
              builder: (ctx) => MaterialButton(
                  child: Icon(Icons.menu, color: Colors.white),
                  onPressed: () {
                    Scaffold.of(ctx).openDrawer();
                  })),
          title: Text("Ordini")),
      body: this.branch == null
          ? Loading(style: Style.LIGHT)
          : Query(
              options: QueryOptions(
                  documentNode: gql(getOrders),
                  variables: {'branchId': this.branch ?? ""}),
              builder: (QueryResult result,
                  {Function refetch, FetchMore fetchMore}) {
                this.refetch = refetch;
                if (result.data != null) {
                  return (OrderList(
                      orders: result.data["allOrders"],
                      setOverlay: this.setOverlay,
                      setBranch: this.setBranch,
                      refresh: refetch));
                }
                if (result.exception != null) {
                  print(result.exception);
                  return Loading(
                      callback: () async => await Navigator.of(context)
                          .pushReplacementNamed('/logout'));
                }
                return Loading(style: Style.LIGHT);
              },
            ),
      floatingActionButton: this.branch != null
          ? FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                Provider.of<AppState>(context, listen: false).selectOrder =
                    null;
                Navigator.of(context).pushNamed('/order');
              })
          : null,
    );
  }

  setBranch(value) {
    WidgetsBinding.instance.addPostFrameCallback((_) => {
          if (value != null)
            {
              setState(() {
                this.branch = value;
                this.overlay = null;
              }),
              Provider.of<AppState>(context, listen: false).branch = this.branch
            }
        });
  }
}
