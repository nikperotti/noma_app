import 'package:NOMA_app/components/loading.dart';
import 'package:NOMA_app/data/model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Logout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Provider.of<AppState>(context).logout();
    WebStorage.instance.token = null;
    WebStorage.instance.userId = null;
    Function callback =
        () async => {Navigator.of(context).pushReplacementNamed('/login')};
    return Loading(callback: callback);
  }
}
