import 'dart:ui';
import 'package:NOMA_app/data/model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Signature extends StatefulWidget {
  Signature({Key key}) : super(key: key);
  @override
  _SignatureState createState() => _SignatureState();
}

class _SignatureState extends State<Signature> {
  AppState appState;
  Color colorLine = Colors.black;
  double widthLine = 3.0;
  double opacityLine = 1.0;
  List<DrawingPoints> points = List();
  StrokeCap strokeCap = StrokeCap
      .round; //(Platform.isAndroid) ? StrokeCap.butt : StrokeCap.round;
  _SignatureState();

  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.appState = Provider.of<AppState>(context);

    return Stack(children: [
      //Lettore di gesture
      Container(
          color: Colors.white,
          child: GestureDetector(
            onPanUpdate: (details) {
              setState(() {
                RenderBox renderBox = context.findRenderObject();
                points.add(DrawingPoints(
                    points: renderBox.globalToLocal(details.globalPosition),
                    paint: Paint()
                      ..strokeCap = strokeCap
                      ..isAntiAlias = true
                      ..color = colorLine.withOpacity(opacityLine)
                      ..strokeWidth = widthLine));
              });
            },
            onPanStart: (details) {
              setState(() {
                RenderBox renderBox = context.findRenderObject();
                points.add(DrawingPoints(
                    points: renderBox.globalToLocal(details.globalPosition),
                    paint: Paint()
                      ..strokeCap = strokeCap
                      ..isAntiAlias = true
                      ..color = colorLine.withOpacity(opacityLine)
                      ..strokeWidth = widthLine));
              });
            },
            onPanEnd: (details) {
              setState(() {
                points.add(null);
              });
            },
            child: CustomPaint(
              size: Size.infinite,
              painter: DrawingPainter(
                pointsList: points,
              ),
            ),
          )),
      //Pulsante salva
      Positioned(
        bottom: 5,
        right: 10,
        child: RaisedButton(
            onPressed: () {
              this.getImage().then((value) => print(value));
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Text("Salva")),
      )
    ]);
  }

  //Firma to png byte
  Future getImage() async {
    PictureRecorder recorder = PictureRecorder();
    Canvas canvas = Canvas(recorder);
    DrawingPainter painter = DrawingPainter(pointsList: points);
    var size = context.size;
    painter.paint(canvas, size);
    var image = await recorder
        .endRecording()
        .toImage(size.width.floor(), size.height.floor());
    return image.toByteData(format: ImageByteFormat.png);
  }
}

class DrawingPainter extends CustomPainter {
  DrawingPainter({this.pointsList});

  List<DrawingPoints> pointsList;
  List<Offset> offsetPoints = List();
  @override
  void paint(Canvas canvas, Size size) {
    for (int i = 0; i < pointsList.length - 1; i++) {
      if (pointsList[i] != null && pointsList[i + 1] != null) {
        canvas.drawLine(pointsList[i].points, pointsList[i + 1].points,
            pointsList[i].paint);
      } else if (pointsList[i] != null && pointsList[i + 1] == null) {
        offsetPoints.clear();
        offsetPoints.add(pointsList[i].points);
        offsetPoints.add(Offset(
            pointsList[i].points.dx + 0.1, pointsList[i].points.dy + 0.1));
        canvas.drawPoints(PointMode.points, offsetPoints, pointsList[i].paint);
      }
    }
  }

  @override
  bool shouldRepaint(DrawingPainter oldDelegate) => true;
}

class DrawingPoints {
  Paint paint;
  Offset points;
  DrawingPoints({this.points, this.paint});
}
