import 'package:NOMA_app/components/orderBuilder.dart';
import 'package:NOMA_app/data/model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Order extends StatefulWidget {
  Order({Key key}) : super(key: key);
  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<Order> {
  AppState appState;
  _OrderState();

  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.appState = Provider.of(context);
    return Scaffold(
        drawerEdgeDragWidth: 0,
        appBar: AppBar(
            leading: Builder(
                builder: (ctx) => MaterialButton(
                    child: Icon(Icons.arrow_back_ios, color: Colors.white),
                    onPressed: () {
                      Navigator.of(context).pop();
                    })),
            title: this.appState.selectOrder == null ||
                    this.appState.selectOrder.id == null
                ? Text("Nuovo Ordine")
                : Text("Modifica Ordine")),
        body: OrderBuilder());
  }
}
