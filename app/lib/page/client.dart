import 'package:NOMA_app/components/clientBuild.dart';
import 'package:NOMA_app/components/loading.dart';
import 'package:NOMA_app/components/orderBuilder.dart';
import 'package:NOMA_app/data/model.dart';
import 'package:NOMA_app/graphql/searchClients.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class Client extends StatefulWidget {
  Client({Key key}) : super(key: key);
  @override
  _ClientState createState() => _ClientState();
}

class _ClientState extends State<Client> {
  AppState appState;
  _ClientState();

  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.appState = Provider.of<AppState>(context);

    return Scaffold(
        drawerEdgeDragWidth: 0,
        appBar: AppBar(
            leading: Builder(
                builder: (ctx) => MaterialButton(
                    child: Icon(Icons.arrow_back_ios, color: Colors.white),
                    onPressed: () {
                      Navigator.of(context).pop();
                    })),
            title: Text("Nuovo Cliente")),
        body: ClientBuild());
  }
}
