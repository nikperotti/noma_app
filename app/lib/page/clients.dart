import 'package:NOMA_app/components/customDrawer.dart';
import 'package:NOMA_app/components/loading.dart';
import 'package:NOMA_app/data/model.dart';
import 'package:NOMA_app/graphql/searchClients.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class Clients extends StatefulWidget {
  Clients({Key key}) : super(key: key);
  @override
  _ClientsState createState() => _ClientsState();
}

class _ClientsState extends State<Clients> {
  AppState appState;
  _ClientsState();

  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.appState = Provider.of<AppState>(context);

    return Scaffold(
        drawerEdgeDragWidth: 0,
        drawer: CustomDrawer(appState: this.appState),
        appBar: AppBar(
            leading: Builder(
                builder: (ctx) => MaterialButton(
                    child: Icon(Icons.menu, color: Colors.white),
                    onPressed: () {
                      Scaffold.of(ctx).openDrawer();
                    })),
            title: Text("Clienti")),
        /*body: Query(
          options: QueryOptions(documentNode: gql(searchClients), variables: {}),
          builder: (QueryResult result, {void refetch, FetchMore fetchMore}) {
            if (result.data != null) {
              print(result.data);
            }
            if (result.exception != null) {
              print(result.exception);
              //return Loading(callback: () async => await Navigator.of(context).pushReplacementNamed('/logout'));
            }
            return Loading(style: Style.LIGHT);
          },
        )*/
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () => Navigator.of(context).pushNamed('/client')));
  }
}
